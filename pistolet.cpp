/* --- includes ----- */
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <SOIL/SOIL.h>
#include <vector>
#include <iostream>
#include <glm/glm.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

/* --- defines ---- */
#define WIDTH 640
#define HEIGHT 640
#define TRUE 1
#define FALSE 0
#define TT 3.14159265358979323846
#define TIMERMSECS 33
#define OBJECTS 5

/* ------ global variables -------- */
static int startTime;
static int prevTime;
static int currTime;
static int timeSincePrevFrame;
static int elapsedTime;

static int lClick = 0;
static int rClick = 0;

static int angle1 = 0, angle2 = 0;
static double move1 = 0.0, move2 = 0.0;

static int oldX=0, oldY=0;

static double anim;
static double pktWspl;
static double zoom = 1.0;

// rozmiary bryły obcinania
static const GLdouble left = - 2.0;
static const GLdouble right = 2.0;
static const GLdouble bottom = - 2.0;
static const GLdouble top = 2.0;
static const GLdouble near = 3.0;
static const GLdouble far = 7.0;
static int smooth_quality = 1;

GLfloat LightAmbient[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat LightDiffuse[] = { 0.7f, 0.7f, 0.7f, 1.0f };
GLfloat LightSpecular[] = { 0.3f, 0.3f, 0.3f, 0.1f };
GLfloat LightPosition[] = { 4.0f, -4.0f, 50.0f, 1.0f }; 

GLuint gunTexture;

GLuint vertexbuffers[OBJECTS];
GLuint uvbuffers[OBJECTS];
GLuint normalbuffers[OBJECTS];
GLuint buffersizes[OBJECTS];

float slideMove=0.0f, triggerMove=0.0f, magazineMove=0.0f, hammerMove=0.0f, bulletMove=0.0f;
int action=0, actionProposition=0;

/* ---- Function Implementations ----- */

bool loadAssImp( const char * path, unsigned int objIndex, std::vector<unsigned short> & indices,
		std::vector<glm::vec3> & vertices, std::vector<glm::vec2> & uvs, std::vector<glm::vec3> & normals )
{
        Assimp::Importer importer;

        const aiScene* scene = importer.ReadFile(path, 0/*aiProcess_JoinIdenticalVertices | aiProcess_SortByPType*/);
        if( !scene) {
                fprintf( stderr, importer.GetErrorString());
                getchar();
                return false;
        }
        const aiMesh* mesh = scene->mMeshes[objIndex];

        // Fill vertices positions
        vertices.reserve(mesh->mNumVertices);
        for(unsigned int i=0; i<mesh->mNumVertices; i++){
                aiVector3D pos = mesh->mVertices[i];
                vertices.push_back(glm::vec3(pos.x, pos.y, pos.z));
        }

        // Fill vertices texture coordinates
        uvs.reserve(mesh->mNumVertices);
        for(unsigned int i=0; i<mesh->mNumVertices; i++){
                aiVector3D UVW = mesh->mTextureCoords[0][i]; // Assume only 1 set of UV coords; AssImp supports 8 UV sets.
                uvs.push_back(glm::vec2(UVW.x, UVW.y));
        }

        // Fill vertices normals
        normals.reserve(mesh->mNumVertices);
        for(unsigned int i=0; i<mesh->mNumVertices; i++){
                aiVector3D n = mesh->mNormals[i];
                normals.push_back(glm::vec3(n.x, n.y, n.z));
        }


        // Fill face indices
        indices.reserve(3*mesh->mNumFaces);
        for (unsigned int i=0; i<mesh->mNumFaces; i++){
                // Assume the model has only triangles.
                indices.push_back(mesh->mFaces[i].mIndices[0]);
                indices.push_back(mesh->mFaces[i].mIndices[1]);
                indices.push_back(mesh->mFaces[i].mIndices[2]);
        }
       
        // The "scene" pointer will be deleted automatically by "importer"

}

static void meshgen( unsigned int objIndex )
{
	glBindBuffer( GL_ARRAY_BUFFER , vertexbuffers[objIndex] );
	glVertexPointer(3, GL_FLOAT, 0, 0);
	glBindBuffer( GL_ARRAY_BUFFER , uvbuffers[objIndex] );
	glTexCoordPointer(2, GL_FLOAT, 0, 0);
	glBindBuffer( GL_ARRAY_BUFFER , normalbuffers[objIndex] );
	glNormalPointer(3, GL_FLOAT, 0);
	glDrawArrays(GL_TRIANGLES, 0, buffersizes[objIndex] );
}

static void bullet()
{
	float x,y, i=0;

	glBegin( GL_QUAD_STRIP );
	for( i=0.0f ; i <= 2.2f ; i+= 0.2f ){
      x = 2.0f * cos( TT * i );
      y = 2.0f * sin( TT * i );
		glTexCoord2f( 0.90234f + i*0.02f, 0.50831f );
      glVertex3f( x, 0.0f+y,  4.0f );
		glTexCoord2f( 0.90234f + i*0.02f, 0.48828f );
      glVertex3f( x, 0.0f+y, -4.0f );
	}
	glEnd();
	glBegin( GL_TRIANGLE_FAN );
	for( i=0.0f ; i <= 2.2f ; i+= 0.2f ){
      x = 2.0f * cos( TT * i );
      y = 2.0f * sin( TT * i );
      glVertex3f( x, 0.0f+y,  4.0f );
	}
	glEnd();
	glBegin( GL_TRIANGLE_FAN );
	for( i=0.0f ; i <= 2.2f ; i+= 0.2f ){
      x = 2.0f * cos( TT * i );
      y = 2.0f * sin( TT * i );
      glVertex3f( x, 0.0f+y, -4.0f );
	}
	glEnd();

}

static void scenegen()
{
	/* 
	 * 0»frame, 1»slide, 2»magazine, 3»trigger, 4»hammer
	 */

	glPushMatrix();
	glTranslated( -5.7f - 48*bulletMove , 15.8f + -45*bulletMove*bulletMove + 21*bulletMove , -3.5f - bulletMove*2.800f );
	bullet();
	glPopMatrix();

	meshgen(0);

	glPushMatrix();
	glTranslated(0,0, -fabs(11.0*slideMove) );
	meshgen(1);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0, -fabs(40*magazineMove), -fabs(12.82*magazineMove) );
	meshgen(2);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0,0, -fabs(2.7*triggerMove) );
	meshgen(3);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0, 11.5, -13.1);
	glRotated( -fabs(65*hammerMove), 1, 0, 0);
	glTranslated(0, -11.8, 13.1);
	meshgen(4);
	glPopMatrix();
}

static int shoot()
{
	static int phase = 0;
	const float speed = 1.2f;

	if( magazineMove > 0 ) return 0;
	if( triggerMove < 0.99f && phase<=1 )
	{
		phase = 1;
		hammerMove = triggerMove += 0.02f * speed;
	}
	else if( hammerMove > 0.01f && phase<=2 )
	{
		phase = 2;
		hammerMove -= 0.12f * speed;
		triggerMove -= 0.06f * speed;
	}
	else if( slideMove < 0.99f && phase<=3 )
	{
		phase = 3;
		if( triggerMove > 0.00f )
			triggerMove -= 0.06f * speed;
		else triggerMove = 0;
		slideMove += 0.14f * speed;
	}
	else if( slideMove > 0.01f && phase<=4 )
	{
		phase = 4;
		slideMove -= 0.04f * speed;
		bulletMove += 0.03 * speed - bulletMove * 0.04f;
	}
	else
	{
		bulletMove = slideMove = hammerMove = phase = 0;
		return 0;
	}
	return 1;
}

static int takeMagazineOff()
{
	static short direction=0, time;
	if( magazineMove < 0.01f && direction==0 )
	{
		direction =  1;
	}
	else if( magazineMove > 0.99f && direction==0 )
	{
		direction = -1;
	}

	magazineMove += 0.03f * direction ;

	if(
			( magazineMove > 0.999f && direction== 1 ) ||
			( magazineMove < 0.001f && direction==-1 )){
		magazineMove = floor( ( direction + 1 ) / 2.0 );
		direction = 0;
		return 0;
	}
	return 2;
}

static void physics()
{
	/* 
	 * list of actions:
	 * 0 » ready to use
	 * 1 » shoot
	 * 2 » take magazine off / put it back
	 */

	switch( action )
	{
		case 0:
			action = actionProposition ;
			break;
		case 1:
			action = shoot();
			break;
		case 2:
			action = takeMagazineOff();
			break;
	}
	actionProposition = 0;
}

static void render()
{
	int min, max, x, y;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	glPushMatrix();

	glTranslated(move1-3, move2, -90.0*zoom);
	glRotated(angle1+30, 1.0, 0.0, 0.0);
	glRotated(angle2+45, 0.0, 1.0, 0.0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, LightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);
	glEnable(GL_LIGHT0);

	glEnable(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, gunTexture);

	glEnableClientState (GL_TEXTURE_COORD_ARRAY);
	glEnableClientState( GL_VERTEX_ARRAY );

	glEnable( GL_MULTISAMPLE );
	glEnable( GL_SAMPLE_COVERAGE );
	glSampleCoverage( 1.0f, GL_FALSE );

//	glBegin( GL_LINES );
//	glColor3f( 1.0f, 0.0f, 0.0f );
//	glVertex3d( -100, 0, 0 );
//	glVertex3d(  100, 0, 0 );
//	glColor3f( 0.0f, 1.0f, 0.0f );
//	glVertex3d( 0, -100, 0 );
//	glVertex3d( 0,  100, 0 );
//	glColor3f( 0.0f, 0.0f, 1.0f );
//	glVertex3d( 0, 0, -100 );
//	glVertex3d( 0, 0,  100 );
//	glEnd();

	physics();

	glPushMatrix();
	scenegen();
	glPopMatrix(); 

	glPopMatrix(); 

	glutSwapBuffers();
	glFlush();
}

static void animate(int value)
{
	glutTimerFunc(TIMERMSECS, animate, 0);
	currTime = glutGet(GLUT_ELAPSED_TIME);
	timeSincePrevFrame = currTime - prevTime;
	elapsedTime = currTime - startTime;

	// zmienne animacji
	anim = ( sin( currTime / 800.0 ) + 1 ) / 2.0;

	glutPostRedisplay();
	prevTime = currTime;
}
	
static void key(unsigned char k, int x, int y)
{
	switch (k) {
		case 27:
			exit(0);
			break;
		case 'k':
			angle1=angle2=move1=move2=0;
			zoom = 1.0;
			break;
		case 'q':
			exit(0);
			break;
		case ' ':
			actionProposition = 1;
			break;
		case 'r':
			actionProposition = 2;
			break;
		default:
			return;
	}
	glutPostRedisplay();
}

static void mouseClick( int button, int state, int x, int y )
{
	   if  ( button == GLUT_LEFT_BUTTON  && state == GLUT_DOWN )  lClick = TRUE;
	else if( button == GLUT_LEFT_BUTTON  && state == GLUT_UP   )  lClick = FALSE;
	else if( button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN )  rClick = TRUE;
	/*  map middle to right */
	else if( button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN )  rClick = TRUE;
	else if( button == GLUT_MIDDLE_BUTTON && state == GLUT_UP   )  rClick = FALSE;
	/*  end of map */
	else if( button == GLUT_RIGHT_BUTTON && state == GLUT_UP   )  rClick = FALSE;
	else if( button == 3 ) zoom -= 0.02;
	else if( button == 4 ) zoom += 0.02;
	oldX = x;
	oldY = y;
}

static void mouseMotion( int x, int y )
{
	if (rClick){
		angle2 += ( x - oldX )/2;
		angle1 += ( y - oldY )/2;
	}
	if (lClick){
		move1 += ( x - oldX )/10.0;
		move2 -= ( y - oldY )/10.0;
	}
	oldX = x;
	oldY = y;
}

static void reshape(GLsizei w, GLsizei h)
{
	GLdouble aspect = (GLdouble)w / (GLdouble)h ;

	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective( 45,  aspect, 1.0, 500.0 );
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glDrawBuffer(GL_BACK);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST); 
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE);
	glutInitWindowSize (WIDTH, HEIGHT);
	glutCreateWindow (argv[0]);

	glClearColor(0.7, 0.7, 1.0, 1.0);
	reshape(WIDTH, HEIGHT);
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glutDisplayFunc(render);
	glutKeyboardFunc(key);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouseClick);
	glutMotionFunc(mouseMotion);
	glutPostRedisplay();
	glutTimerFunc(TIMERMSECS, animate, 0);

	// load obj
	glewInit();
	gunTexture = SOIL_load_OGL_texture( "./Tex_0008_1.bmp", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS );

	glGenBuffers(OBJECTS, vertexbuffers);
	glGenBuffers(OBJECTS, uvbuffers);
	glGenBuffers(OBJECTS, normalbuffers);

	for( int i=0; i<OBJECTS; i++ )
	{
		std::vector<unsigned short> indices;
		std::vector<glm::vec3> vertices;
		std::vector<glm::vec2> uvs;
		std::vector<glm::vec3> normals;
		loadAssImp("1911.obj", i, indices, vertices, uvs, normals);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffers[i]);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffers[i]);
		glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffers[i]);
		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
		buffersizes[i] = vertices.size();
	}


	startTime = glutGet(GLUT_ELAPSED_TIME);
	prevTime = startTime;
	glutMainLoop();

	return 0;
}
