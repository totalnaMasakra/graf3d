#include <GL/glut.h>
#include <GL/gl.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <SOIL/SOIL.h>
#include "pistrend.h"

#define WIDTH 640
#define HEIGHT 640

#define TRUE 1
#define FALSE 0

#define TIMERMSECS 33

int startTime;
int prevTime;

int lClick = 0;
int rClick = 0;

int oldX=0, oldY=0;

double anim, animOn=1;

// rozmiary bryły obcinania
const GLdouble left = - 2.0;
const GLdouble right = 2.0;
const GLdouble bottom = - 2.0;
const GLdouble top = 2.0;
const GLdouble near = 3.0;
const GLdouble far = 7.0;
static int smooth_quality = 1;


/* ---- Function Implementations ----- */

static void animate(int value)
{
	glutTimerFunc(TIMERMSECS, animate, 0);
	int currTime = glutGet(GLUT_ELAPSED_TIME);
	int timeSincePrevFrame = currTime - prevTime;
	int elapsedTime = currTime - startTime;

	// zmienne animacji
	anim = sin( currTime / 800.0 ) *animOn;

	glutPostRedisplay();
	prevTime = currTime;
}

static void key(unsigned char k, int x, int y)
{
	switch (k) {
		case 27:
			exit(0);
			break;
		case ' ':
			angle1=angle2=move1=move2=0;
			zoom = 1.0;
			break;
		case 'a':
			if(animOn!=0) animOn = 0;
			else animOn = 1;
			break;
		case 'q':
			exit(0);
		default:
			return;
	}
	glutPostRedisplay();
}

static void mouseClick( int button, int state, int x, int y )
{
	   if  ( button == GLUT_LEFT_BUTTON  && state == GLUT_DOWN )  lClick = TRUE;
	else if( button == GLUT_LEFT_BUTTON  && state == GLUT_UP   )  lClick = FALSE;
	else if( button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN )  rClick = TRUE;
	else if( button == GLUT_RIGHT_BUTTON && state == GLUT_UP   )  rClick = FALSE;
	/* map middle to right */
	else if( button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN )  rClick = TRUE;
	else if( button == GLUT_MIDDLE_BUTTON && state == GLUT_UP   )  rClick = FALSE;
	/* end of map */
	else if( button == 3 ) zoom -= 0.02;
	else if( button == 4 ) zoom += 0.02;
	oldX = x;
	oldY = y;
}

static void mouseMotion( int x, int y )
{
	if (rClick){
		angle2 += ( x - oldX )/2;
		angle1 += ( y - oldY )/2;
	}
	if (lClick){
		move1 += ( x - oldX )/40.0;
		move2 -= ( y - oldY )/40.0;
	}
	oldX = x;
	oldY = y;
}

static void reshape(GLsizei w, GLsizei h)
{
	GLdouble aspect = (GLdouble)w / (GLdouble)h ;

	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective( 45,  aspect, 1.0, 100.0 );
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glDrawBuffer(GL_BACK);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST); 
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE);
	glutInitWindowSize (WIDTH, HEIGHT);
	glutCreateWindow (argv[0]);

	glClearColor(0.0, 0.0, 0.0, 1.0);
	reshape(WIDTH, HEIGHT);
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glutDisplayFunc(render);
	glutKeyboardFunc(key);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouseClick);
	glutMotionFunc(mouseMotion);
	glutPostRedisplay();
	glutTimerFunc(TIMERMSECS, animate, 0);

	//springTexture = SOIL_load_OGL_texture( "./amelynum.bmp", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS );

	startTime = glutGet(GLUT_ELAPSED_TIME);
	prevTime = startTime;
	glutMainLoop();

	return 0;
}
