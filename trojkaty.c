/* vim config
:map <F5> :wall<CR> :make<CR>
:map <F6> :!./trojkaty<CR>
 */

#include <GL/glut.h>
#include <GL/gl.h>
#include <stdlib.h>
#include <math.h>

#define WIDTH 640
#define HEIGHT 640

#define TIMERMSECS 33

#define ROTRATE 45.0f

#define MAXDIST 45.0f

#define PI 3.14
#define HSIZE 40.0f
#define HSIZESQRT HSIZE*1.7321/2
#define TIMERSECS 1

int startTime;
int prevTime;

int rotCent=1,rotMain=1,distC=1;

static GLfloat rot = 0.0f;
static GLfloat dist = 0.0f;

typedef struct
{
	float red;
	float green;
	float blue;
} myColor;
myColor yellow={1.0f,1.0f,0},orange={1.0f,0.5f,0},green={0,1.0f,0},blue={0,0,1.0f},red={1.0f,0,0},purple={1.0f,0,1.0f} ;

// ---- Function Implementations -----

void drawOrtho()
{
	double i;
	glBegin(GL_LINES);
	glColor3f( 0.0f , 0.0f , 0.0f );
	glVertex2f( 0.0f, 500.0f );
	glVertex2f( 0.0f, -500.0f );
	glEnd();

	glBegin(GL_LINES);
	glColor3f( 0.0f , 0.0f , 0.0f );
	glVertex2f( 500.0f, 0.0f );
	glVertex2f( -500.0f, 0.0f );
	glEnd();

	/*
	glBegin( GL_POLYGON );
	for(i = 0; i < 2 * PI; i += PI / 18)
		glVertex3f(cos(i) * HSIZE, sin(i) * HSIZE, 0.0);
	glEnd();

	glBegin( GL_POLYGON );
	for(i = 0; i < 2 * PI; i += PI / 18)
		glVertex3f(cos(i) * (HSIZE + HSIZE), sin(i) * (HSIZE + HSIZE), 0.0);
	glEnd();

	glBegin( GL_POLYGON );
	for(i = 0; i < 2 * PI; i += PI / 18)
		glVertex3f(cos(i) * (HSIZE + 2*HSIZE), sin(i) * (HSIZE + 2*HSIZE), 0.0);
	glEnd();
	*/
}

static void drawTriangle( float x, float y, float angle, int direction, myColor color )
{
	glPushMatrix();

	glRotatef( rot*direction*rotCent, 0.0f, 0.0f, 1.0f );
	glTranslatef( x, y, 0.0f );
	glRotatef( angle, 0.0f, 0.0f, 1.0f );
	glTranslatef( dist*distC, dist*distC, 0.0f );
	glRotatef( rot*direction*rotMain, 0.0f, 0.0f, 1.0f );

	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	glBegin(GL_POLYGON);
	glColor3f( color.red, color.green, color.blue );
	glVertex2f( -HSIZE, HSIZE );
	glVertex2f( -HSIZE, -HSIZE );
	glVertex2f( HSIZE, -HSIZE );
	glEnd();

	glPolygonMode(GL_FRONT_AND_BACK,GL_LINE); 
	glBegin(GL_POLYGON); 
	glColor3f( 0.0f , 0.0f , 0.0f ); 
	glVertex2f( -HSIZE, HSIZE );
	glVertex2f( -HSIZE, -HSIZE );
	glVertex2f( HSIZE, -HSIZE );
	glEnd(); 

	glPopMatrix();

}

static void render()
{
	// Render the screen

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Move and rotate the triangle
	glLoadIdentity();

	glClear(GL_COLOR_BUFFER_BIT);

	//drawOrtho();

	drawTriangle(  HSIZE,  HSIZE,   0.0f, 1, blue );
	drawTriangle( -HSIZE,  HSIZE,  90.0f, 1, blue );
	drawTriangle( -HSIZE, -HSIZE, 180.0f, 1, blue );
	drawTriangle(  HSIZE, -HSIZE, 270.0f, 1, blue );

	drawTriangle( 3* HSIZE,  HSIZE,   0.0f, -1, green );
	drawTriangle( 3*-HSIZE,  HSIZE,  90.0f, -1, orange );
	drawTriangle( 3*-HSIZE, -HSIZE, 180.0f, -1, green );
	drawTriangle( 3* HSIZE, -HSIZE, 270.0f, -1, orange );
	drawTriangle(  HSIZE, 3* HSIZE,   0.0f, -1, orange );
	drawTriangle( -HSIZE, 3* HSIZE,  90.0f, -1, green );
	drawTriangle( -HSIZE, 3*-HSIZE, 180.0f, -1, orange );
	drawTriangle(  HSIZE, 3*-HSIZE, 270.0f, -1, green );

	drawTriangle( 5* HSIZE,  HSIZE,   0.0f, 1, purple );
	drawTriangle( 5*-HSIZE,  HSIZE,  90.0f, 1, red );
	drawTriangle( 5*-HSIZE, -HSIZE, 180.0f, 1, purple );
	drawTriangle( 5* HSIZE, -HSIZE, 270.0f, 1, red );
	drawTriangle(  HSIZE, 5* HSIZE,   0.0f, 1, red );
	drawTriangle( -HSIZE, 5* HSIZE,  90.0f, 1, purple );
	drawTriangle( -HSIZE, 5*-HSIZE, 180.0f, 1, red );
	drawTriangle(  HSIZE, 5*-HSIZE, 270.0f, 1, purple );
	drawTriangle( 3* HSIZE, 3* HSIZE,   0.0f, 1, yellow );
	drawTriangle( 3*-HSIZE, 3* HSIZE,  90.0f, 1, yellow );
	drawTriangle( 3*-HSIZE, 3*-HSIZE, 180.0f, 1, yellow );
	drawTriangle( 3* HSIZE, 3*-HSIZE, 270.0f, 1, yellow );

	// Swap the buffers (if double-buffered) to show the rendered image
	glutSwapBuffers();
	glFlush();
}

static void animate(int value)
{
	// Set up the next timer tick (do this first)
	glutTimerFunc(TIMERMSECS, animate, 0);

	// Measure the elapsed time
	int currTime = glutGet(GLUT_ELAPSED_TIME);
	int timeSincePrevFrame = currTime - prevTime;
	int elapsedTime = currTime - startTime;

	// ##### REPLACE WITH YOUR OWN GAME/APP MAIN CODE HERE #####

	// Rotate the triangle
	rot = (ROTRATE / 1000) * elapsedTime;
	dist = MAXDIST * (sin( elapsedTime / 500.0 )+1);

	// ##### END OF GAME/APP MAIN CODE #####



	// Force a redisplay to render the new image
	glutPostRedisplay();

	prevTime = currTime;
}

static void key(unsigned char k, int x, int y)
{
	switch (k) {
		case 27:
			exit(0);
			break;
		case 0x71:
			if( rotCent == 0 ) rotCent = 1;
			else rotCent = 0;
			break;
		case 0x77:
			if( rotMain == 0 ) rotMain = 1;
			else rotMain = 0;
			break;
		case 0x65:
			if( distC == 0 ) distC = 1;
			else distC = 0;
			break;
		default:
			return;
	}

	// Force a redraw of the screen in order to update the display
	glutPostRedisplay();
}

static void reshape(GLsizei w, GLsizei h)
{
	// Respond to a window resize event

	// ##### REPLACE WITH YOUR OWN RESHAPE CODE #####
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Calculate the aspect ratio of the screen, and set up the
	// projection matrix (i.e., update the camera)
	//gluPerspective(45.0f,(GLfloat)w/(GLfloat)h,0.1f,100.0f);
	gluOrtho2D(-500.0, 500.0, -500.0, 500.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	// ##### END OF RESHAPE CODE #####
}

static void init(int w, int h)
{
	// Set up the OpenGL state
	// ##### REPLACE WITH YOUR OWN OPENGL INIT CODE HERE #####
	glClearColor(1.0, 1.0, 1.0, 1.0);
	reshape(w, h);

	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// ##### END OF INIT CODE #####
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);

	// ##### INSERT ANY ARGUMENT (PARAMETER) PARSING CODE HERE

	// Set up the GLUT window
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize (WIDTH, HEIGHT);
	glutCreateWindow (argv[0]);

	// Set up the callbacks
	init(WIDTH, HEIGHT);
	glutDisplayFunc(render);
	glutKeyboardFunc(key);
	glutReshapeFunc(reshape);
	glutPostRedisplay();
	// ##### INSERT YOUR OWN EXTRA CALLBACK HOOKS HERE #####

	// Start the timer
	glutTimerFunc(TIMERMSECS, animate, 0);

	// Initialize the time variables
	startTime = glutGet(GLUT_ELAPSED_TIME);
	prevTime = startTime;

	// Start the main loop
	glutMainLoop();

	return 0;
}
