#CFLAGS=-ggdb -Wall -pedantic -D_REENTRANT
CFLAGS=-ggdb -D_REENTRANT -lGL -lGLU -lglut -lGLEW -lm -lSOIL -lassimp

%: %.c
	gcc $(CFLAGS) -o $@ $^

%: %.cpp
	g++ $(CFLAGS) -o $@ $^

EXECS= prog1 trojkaty sprezyna pistol pistolet
all: $(EXECS)

prog1: prog1.c
trojkaty: trojkaty.c
sprezyna: sprezyna.c
pistol: pistol.c pistrend.c
pistolet: pistolet.cpp

.PHONY: clean all

clean:
	rm -f $(EXECS)
