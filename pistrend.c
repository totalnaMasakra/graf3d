#include "pistrend.h"

int angle1 = 0, angle2 = 0;
double move1 = 0.0, move2 = 0.0;
double zoom = 1.0;

GLfloat LightAmbient[] = { 0.2f, 0.2f, 0.2f, 0.2f };
GLfloat LightDiffuse[] = { 0.7f, 0.7f, 0.7f, 1.0f };
GLfloat LightSpecular[] = { 0.5f, 0.5f, 0.5f, 0.1f };
GLfloat LightPosition[] = { 4.0f, -4.0f, 5.0f, 1.0f }; 

GLfloat WhiteSurface[] = { 1.0f, 1.0f, 1.0f, 1.0f};
GLfloat GreySurface[] = { 0.3f, 0.3f, 0.3f, 1.0f};
GLfloat RedSurface[] = { 1.0f, 0.0f, 0.0f, 1.0f};
GLfloat GreenSurface[] = { 0.0f, 1.0f, 0.0f, 1.0f};
GLfloat BlueSurface[] = { 0.0f, 0.0f, 1.0f, 1.0f};

void render()
{
	int min, max, x, y;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	glPushMatrix();

	glTranslated(move1, move2, -16.0*zoom);
	glRotated(angle1, 1.0, 0.0, 0.0);
	glRotated(angle2, 0.0, 1.0, 0.0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, LightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);
	glEnable(GL_LIGHT0);

	glEnable(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glEnable( GL_MULTISAMPLE );
	glEnable( GL_SAMPLE_COVERAGE );
	glSampleCoverage( 0.8f, GL_FALSE );

	/*
	 * generating elements
	 *
	 * ~~ axis legend ~~
	 * x - depth
	 * y - height
	 * z - width
	 * ~~ on the front of the gun ~~
	 *
	 */
	glPushMatrix();
//	glTranslated( 0.0f, 5.0f, 0.0f );
	slide();
	glPopMatrix();
	frame();

	glPopMatrix(); 

	glutSwapBuffers();
	glFlush();
}

void slide()
{
	float x,y,z,i; /* x,y,z-axis model transformation and iterator */
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, GreySurface);

	/* slide body */
	glBegin( GL_QUAD_STRIP );
	// right bottom side
	for( i=-1.0f ; i < -0.3f ; i+= 0.05f ){
		y = i;
		z = log2( -i ) ;
		glVertex3f( 0.5f , 0.0f+y ,  2.0f - z );
		glVertex3f( 0.5f , 0.0f+y , -4.0f + 0.3f*i );
	}
	// right side
	for( i=-0.3f ; i < 0.0f ; i+= 0.1f ){
		y = i;
		glVertex3f( 0.5f , 0.0f+y ,  4.0f );
		glVertex3f( 0.5f , 0.0f+y , -4.0f + 0.3f*i );
	}
	// top curve without ejection hole
	for( i=0.0f ; i <= 0.41f ; i+= 0.1f ){ 
		x = 0.5f * cos( TT * i );
		y = 0.5f * sin( TT * i );
		z = 0.3f*y ;
		glVertex3f( x, 0.0f+y,  4.0f );
		glVertex3f( x, 0.0f+y, -4.0f + z );
	}
	glEnd();
	// ~~ top curve left side, ejection hole rear ~~
	glBegin( GL_QUAD_STRIP );
	for( i=0.4f ; i < 1.1f ; i+= 0.1f ){ 
		x = 0.5f * cos( TT * i );
		y = 0.5f * sin( TT * i );
		z = 0.3f*y ;
		glVertex3f( x, 0.0f+y, -1.5f );
		glVertex3f( x, 0.0f+y, -4.0f + z );
	}
	glEnd();
	glBegin( GL_QUAD_STRIP );
	for( i=0.4f ; i < 1.1f ; i+= 0.1f ){ 
		x = 0.5f * cos( TT * i );
		y = 0.5f * sin( TT * i );
		z = 0.3f*y ;
		glVertex3f( x, 0.0f+y,  4.0f );
		glVertex3f( x, 0.0f+y,  0.0f );
	}
	glEnd();
	// left side
	glBegin( GL_QUAD_STRIP );
	for( i=0.0f ; i > -0.4f ; i-= 0.1f ){
		y = i;
		glVertex3f( -0.5f , 0.0f+y ,  4.0f );
		glVertex3f( -0.5f , 0.0f+y , -4.0f + 0.3f*i );
	}
	// left bottom side
	for( i=-0.4f ; i > -1.01f ; i-= 0.05f ){
		y = i;
		z = log2( -i ) ;
		glVertex3f( -0.5f , 0.0f+y ,  2.0f - z );
		glVertex3f( -0.5f , 0.0f+y , -4.0f + 0.3f*i );
	}
	// closing polygon
	glVertex3f( 0.5f , -1.0f ,  2.0f - log2( 1 ) );
	glVertex3f( 0.5f , -1.0f , -4.0f - 0.3f );
	glEnd();

	/* rear side */
	glBegin( GL_TRIANGLE_STRIP );
	// lower part
	for( i= -1.0f ; i< 0.0f ; i += 0.05f ){
		glVertex3f( 0.5f , i , -4.0f + 0.3f*i );
		glVertex3f( -0.5f , i , -4.0f + 0.3f*i );
	}
	// higher part
	for( i=0.0f ; i < 0.55f ; i+= 0.1f ){ 
		x = 0.5f * cos( TT * i );
		y = 0.5f * sin( TT * i );
		z = 0.3f*y ;
		glVertex3f( x, 0.0f+y, -4.0f + z );
		glVertex3f( -x, 0.0f+y, -4.0f + z );
	}
	glEnd();
	
	/* front */
	// inset right
	glBegin( GL_TRIANGLE_STRIP );
	for( i=-1.0f ; i < -0.3f ; i+= 0.05f ){
		y = i;
		z = log2( -i ) ;
		glVertex3f( 0.4f , 0.0f+y ,  2.0f - z );
		glVertex3f( 0.4f , 0.0f+y , 4.0f );
	}
	glEnd();
	// connection right
	glBegin( GL_TRIANGLE_STRIP );
	for( i=-1.0f ; i < -0.3f ; i+= 0.05f ){
		y = i;
		z = log2( -i ) ;
		glVertex3f( 0.5f , 0.0f+y ,  2.0f - z );
		glVertex3f( 0.4f , 0.0f+y ,  2.0f - z );
	}
	glVertex3f( 0.5f , -0.30f ,  4.0f );
	glVertex3f( 0.4f , -0.35f ,  4.0f );
	glEnd();
	// inset left
	glBegin( GL_TRIANGLE_STRIP );
	for( i=-1.0f ; i < -0.3f ; i+= 0.05f ){
		y = i;
		z = log2( -i ) ;
		glVertex3f( -0.4f , 0.0f+y ,  2.0f - z );
		glVertex3f( -0.4f , 0.0f+y , 4.0f );
	}
	glEnd();
	// connection left
	glBegin( GL_TRIANGLE_STRIP );
	for( i=-1.0f ; i < -0.3f ; i+= 0.05f ){
		y = i;
		z = log2( -i ) ;
		glVertex3f( -0.5f , 0.0f+y ,  2.0f - z );
		glVertex3f( -0.4f , 0.0f+y ,  2.0f - z );
	}
	glVertex3f( -0.5f , -0.30f ,  4.0f );
	glVertex3f( -0.4f , -0.35f ,  4.0f );
	glEnd();
	// round bottom
	glBegin( GL_TRIANGLE_STRIP );
	for( i=0.0f ; i > -1.1f ; i-= 0.1f ){ 
		x = 0.4f * cos( TT * i );
		y = 0.4f * sin( TT * i );
		z = 0.3f*y ;
		glVertex3f( x, -1.0f+y, 2.0f );
		glVertex3f( x, -1.0f+y, 4.0f );
	}
	glEnd();
	// front half round
	glBegin( GL_TRIANGLE_FAN );
	for( i=0.0f ; i > -1.1f ; i-= 0.1f ){ 
		x = 0.4f * cos( TT * i );
		y = 0.4f * sin( TT * i );
		z = 0.3f*y ;
		glVertex3f( x, -1.0f+y, 4.0f );
	}
	glEnd();
	// front triangles
	glBegin( GL_TRIANGLE_STRIP );
	for( i=-1.0f ; i < 0.66f ; i+= 0.05f ){
		y = 0.4f * i ;
		glVertex3f(  0.4f, -0.6f+y, 4.0f );
		glVertex3f( -0.4f, -0.6f+y, 4.0f );
	}
	for( i=0.66f ; i < 1.25f ; i+= 0.05f ){
		y = 0.5f * i ;
		glVertex3f(  0.5f, -0.6f+y, 4.0f );
		glVertex3f( -0.5f, -0.6f+y, 4.0f );
	}
	glEnd();
}

void frame()
{
	float x,y,z,i; /* x,y,z-axis model transformation and iterator */
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, RedSurface);
	
}
