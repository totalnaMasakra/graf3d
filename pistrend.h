#ifndef _PISTREND_H_
#define _PISTREND_H_

#include <GL/glut.h>
#include <GL/gl.h>
#include <stdio.h>
#include <math.h>

#define TT 3.14159265358979323846 

extern int angle1, angle2;
extern double move1, move2;
extern double zoom;

extern GLfloat LightAmbient[];
extern GLfloat LightDiffuse[];
extern GLfloat LightSpecular[];
extern GLfloat LightPosition[]; 

extern GLfloat WhiteSurface[];
extern GLfloat GreySurface[];
extern GLfloat RedSurface[];
extern GLfloat GreenSurface[];
extern GLfloat BlueSurface[];

void render();
void slide();
void frame();

#endif
