#include <GL/glut.h>
#include <GL/gl.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <SOIL/SOIL.h>

#define WIDTH 640
#define HEIGHT 640

#define TRUE 1
#define FALSE 0

#define TT 3.14159265358979323846

#define TIMERMSECS 33

static int startTime;
static int prevTime;

static int lClick = 0;
static int rClick = 0;

static int angle1 = 0, angle2 = 0;
static double move1 = 0.0, move2 = 0.0;

static int oldX=0, oldY=0;

static double anim, animOn=1;
static double pktWspl;
static double zoom = 1.0;

// rozmiary bryły obcinania
static const GLdouble left = - 2.0;
static const GLdouble right = 2.0;
static const GLdouble bottom = - 2.0;
static const GLdouble top = 2.0;
static const GLdouble near = 3.0;
static const GLdouble far = 7.0;
static int smooth_quality = 1;

GLfloat WhiteSurface[] = { 1.0f, 1.0f, 1.0f, 1.0f};
GLfloat GreySurface[] = { 0.3f, 0.3f, 0.3f, 1.0f};
GLfloat RedSurface[] = { 1.0f, 0.0f, 0.0f, 1.0f};
GLfloat GreenSurface[] = { 0.0f, 1.0f, 0.0f, 1.0f};
GLfloat BlueSurface[] = { 0.0f, 0.0f, 1.0f, 1.0f};
GLfloat LightAmbient[] = { 0.2f, 0.2f, 0.2f, 0.2f };
GLfloat LightDiffuse[] = { 0.7f, 0.7f, 0.7f, 1.0f };
GLfloat LightSpecular[] = { 0.0f, 0.0f, 0.0f, 0.1f };
GLfloat LightPosition[] = { 4.0f, -4.0f, 5.0f, 1.0f }; 

GLuint springTexture;
GLuint ballTexture;

/* ---- Function Implementations ----- */

static void sufit()
{
	glBindTexture( GL_TEXTURE_2D, 0 );
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, GreySurface);
	glBegin(GL_QUADS);
	glVertex3d( -20 , 0 , -20 );
	glVertex3d( -20 , 0 ,  20 );
	glVertex3d(  20 , 0 ,  20 );
	glVertex3d(  20 , 0 , -20 );
	glEnd();
}

static void sprezyna()
{
	double t, u, tJump, uJump, sHeight, sCentr, sDiag, sShrink;
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, GreySurface);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 1.0f);

	glBindTexture( GL_TEXTURE_2D, springTexture );

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

	glBegin(GL_QUAD_STRIP);
	glNormal3d( 1.0, 0.0, 0.0);
	sHeight = 0.18+ 0.06*anim;
	sCentr = 1;
	sDiag = 0.17;
	tJump=8*TT / 128;
	uJump=2*TT / 64;
	for ( u=0; u< 2*TT; u+= uJump ) {
		glTexCoord3f(
				sin( t )*( sCentr/sShrink + cos(u)*sDiag ) / 10,
				cos( t )*( sCentr/sShrink + cos(u)*sDiag ) / 10,
				sin( sHeight*t + sin(u)*sDiag ) / 10
				);
		/* od dołu do góry na zwężeniu */
		for ( t=-9.5*TT; t<-8*TT-tJump ; t+= tJump ) {
//			sShrink = 1 - (t+8*TT)/2.0*TT;
			sShrink = 1 - (t+8*TT)/2.0*TT;
			glVertex3d(
					cos( t )*( sCentr/sShrink + cos(u)*sDiag ),
					sHeight*t + sin(u)*sDiag,
					sin( t )*( sCentr/sShrink + cos(u)*sDiag )
					);
			glVertex3d(
					cos(t)*( sCentr/sShrink + cos(u+uJump)*sDiag ),
					sHeight*(t) + sin(u+uJump)*sDiag,
					sin(t)*( sCentr/sShrink + cos(u+uJump)*sDiag )
					);
		}
		/* od dołu do góry normalnie */
		for ( t=-8*TT; t<0 ; t+= tJump ) {
			glVertex3d(
					cos( t )*( sCentr+cos(u)*sDiag ),
					sHeight*t + sin(u)*sDiag,
					sin( t )*( sCentr+cos(u)*sDiag )
					);
			glVertex3d(
					cos(t)*( sCentr+cos(u+uJump)*sDiag ),
					sHeight*(t) + sin(u+uJump)*sDiag,
					sin(t)*( sCentr+cos(u+uJump)*sDiag )
					);
		}
		/* od góry do dołu normalnie */
		for ( t=0; t>-8*TT ; t-= tJump ) {
			glVertex3d(
					cos( t )*( sCentr+cos(u)*sDiag ),
					sHeight*t + sin(u)*sDiag,
					sin( t )*( sCentr+cos(u)*sDiag )
					);
			glVertex3d(
					cos(t)*( sCentr+cos(u+uJump)*sDiag ),
					sHeight*(t) + sin(u+uJump)*sDiag,
					sin(t)*( sCentr+cos(u+uJump)*sDiag )
					);
		}
		/* od góry do dołu na zwężeniu */
		for ( t=-8*TT-tJump; t> -9.5*TT ; t-= tJump ) {
			sShrink = 1 - (t+8*TT)/2.0*TT;
			glVertex3d(
					cos( t )*( sCentr/sShrink + cos(u)*sDiag ),
					sHeight*t + sin(u)*sDiag,
					sin( t )*( sCentr/sShrink + cos(u)*sDiag )
					);
			glVertex3d(
					cos(t)*( sCentr/sShrink + cos(u+uJump)*sDiag ),
					sHeight*(t) + sin(u+uJump)*sDiag,
					sin(t)*( sCentr/sShrink + cos(u+uJump)*sDiag )
					);
		}
	}
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, GreenSurface);
	glEnd();
	pktWspl = sHeight*(t+tJump) + sin(u)*sDiag;
}

static void kula()
{
	GLUquadricObj *quadric;

	glPushMatrix();
	glTranslatef( 0, pktWspl-0.8 ,0 );

	quadric=gluNewQuadric();
	gluQuadricNormals(quadric, GLU_SMOOTH);
	gluQuadricTexture(quadric, GL_TRUE);

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, WhiteSurface);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 0.0f);

	glEnable(GL_TEXTURE_2D);
	glBindTexture( GL_TEXTURE_2D, ballTexture );

	gluSphere(quadric, 1.0f, 35,35);

	glPopMatrix();
}

static void render()
{
	int min, max, x, y;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	glPushMatrix();

	glTranslated(move1, 5.0+move2, -16.0*zoom);
	glRotated(angle1, 1.0, 0.0, 0.0);
	glRotated(angle2, 0.0, 1.0, 0.0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, LightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);
	glEnable(GL_LIGHT0);

	glEnable(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glEnable( GL_MULTISAMPLE );
	glEnable( GL_SAMPLE_COVERAGE );
	glSampleCoverage( 0.8f, GL_FALSE );


//	sufit();
	sprezyna();
	kula();

	glPopMatrix(); 

	glutSwapBuffers();
	glFlush();
}

static void animate(int value)
{
	glutTimerFunc(TIMERMSECS, animate, 0);
	int currTime = glutGet(GLUT_ELAPSED_TIME);
	int timeSincePrevFrame = currTime - prevTime;
	int elapsedTime = currTime - startTime;

	// zmienne animacji
	anim = sin( currTime / 800.0 ) *animOn;

	glutPostRedisplay();
	prevTime = currTime;
}

static void key(unsigned char k, int x, int y)
{
	switch (k) {
		case 27:
			exit(0);
			break;
		case ' ':
			angle1=angle2=move1=move2=0;
			zoom = 1.0;
			break;
		case 'a':
			if(animOn!=0) animOn = 0;
			else animOn = 1;
			break;
		case 'q':
			exit(0);
		default:
			return;
	}
	glutPostRedisplay();
}

static void mouseClick( int button, int state, int x, int y )
{
	   if  ( button == GLUT_LEFT_BUTTON  && state == GLUT_DOWN )  lClick = TRUE;
	else if( button == GLUT_LEFT_BUTTON  && state == GLUT_UP   )  lClick = FALSE;
	else if( button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN )  rClick = TRUE;
	else if( button == GLUT_RIGHT_BUTTON && state == GLUT_UP   )  rClick = FALSE;
	else if( button == 3 ) zoom -= 0.02;
	else if( button == 4 ) zoom += 0.02;
	oldX = x;
	oldY = y;
}

static void mouseMotion( int x, int y )
{
	if (rClick){
		angle2 += ( x - oldX )/2;
		angle1 += ( y - oldY )/2;
	}
	if (lClick){
		move1 += ( x - oldX )/40.0;
		move2 -= ( y - oldY )/40.0;
	}
	oldX = x;
	oldY = y;
}

static void reshape(GLsizei w, GLsizei h)
{
	GLdouble aspect = (GLdouble)w / (GLdouble)h ;

	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective( 45,  aspect, 1.0, 100.0 );
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glDrawBuffer(GL_BACK);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST); 
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE);
	glutInitWindowSize (WIDTH, HEIGHT);
	glutCreateWindow (argv[0]);

	glClearColor(0.0, 0.0, 0.0, 1.0);
	reshape(WIDTH, HEIGHT);
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glutDisplayFunc(render);
	glutKeyboardFunc(key);
	glutReshapeFunc(reshape);
	glutMouseFunc(mouseClick);
	glutMotionFunc(mouseMotion);
	glutPostRedisplay();
	glutTimerFunc(TIMERMSECS, animate, 0);

	springTexture = SOIL_load_OGL_texture( "./amelynum.bmp", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS );
	ballTexture = SOIL_load_OGL_texture( "./tenis.bmp", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS );

	startTime = glutGet(GLUT_ELAPSED_TIME);
	prevTime = startTime;
	glutMainLoop();

	return 0;
}
